import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';
import { AboutComponent } from './about/about.component';
import { FeaturesComponent } from './features/features.component';
import {AgTable} from './partial/ag-table';
import {AgGridModule} from 'ag-grid-angular';
import {FeatureService} from './provider/feature-service';
import { downloadNodin, deleteNodin, nodinComponent } from './partial/nodin';
import { AgGridTableComponent } from './partial/ag-grid-table';
import { LukeComponent } from './modal/modalUpload';
import { ModalTable } from './partial/modal-tabel';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ModalFormComponent } from './modal-form/modal-form';
import { unknownTable } from './partial/unknown-table';
import { BasicDemoComponent } from './partial/handsontable/hands-table';
import { HotTableModule } from '@handsontable/angular';
import { testingTable } from './partial/testing-table';

@NgModule({
  imports: [
    SharedModule, 
    StaticRoutingModule, 
    AgGridModule,
    // AgGridTableComponent,
    AgGridModule.withComponents([AgGridTableComponent]),
    MDBBootstrapModule.forRoot(),
    HotTableModule.forRoot(),
  ],
  declarations: [
    AboutComponent, 
    FeaturesComponent, 
    AgTable,
    downloadNodin,
    deleteNodin,
    nodinComponent,
    AgGridTableComponent,
    ModalTable,
    LukeComponent,
    ModalFormComponent,
    unknownTable,
    BasicDemoComponent,
    testingTable,
    
    // MDBBootstrapModule.forRoot(),
  ],
  providers: [
    FeatureService,
    downloadNodin,
    deleteNodin,
    nodinComponent,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class StaticModule {}
