import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';

import { SettingsModule } from './settings';
import { StaticModule } from './static';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {AgGridModule} from 'ag-grid-angular';
import { FormsModule, FormControl, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCheckboxModule, MatChipsModule } from '@angular/material';

@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatChipsModule,

    

    // core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,
    SettingsModule,
   

    // app
    AppRoutingModule,

    // agGrid
    AgGridModule.withComponents([]),
    FormsModule,
    HttpClientModule,
    
    MDBBootstrapModule.forRoot(),
  ],
  declarations: [AppComponent, ],
  providers: [
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA,  ]
})
export class AppModule {}


