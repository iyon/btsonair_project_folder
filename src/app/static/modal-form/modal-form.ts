import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'modal-form',
  templateUrl: './modal-form.html',
  styleUrls: ['./modal-form.css']
})
export class ModalFormComponent {
  signupFormModalName = new FormControl('', Validators.required);
  signupFormModalEmail = new FormControl('', Validators.email);
  signupFormModalPassword = new FormControl('', Validators.required);
}