import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturesComponent } from './features/features.component';

const routes: Routes = [
  // { path: 'cacti', component: AboutComponent, data: { title: 'Quick Cacti' } },
  // {
  //   path: 'features',
  //   component: FeaturesComponent,
  //   data: { title: 'Features' }
  // }
  {
    path: 'btsonair',
    component: FeaturesComponent,
    // data: { title: 'BTS ON AIR' }
    data: { title: 'NEISA | BTS ON AIR' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule {}
