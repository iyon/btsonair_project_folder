import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import {GridOptions} from 'ag-grid';
import { AgHeaders } from './common.model';


@Component({
  selector: 'ag-grid-table',
  template: `
    <!--<div class="row">-->
      <!--<div class="col-md-12" style="text-align: left">-->
        <!--<div style="-->
        <!--margin-bottom: -1%; -->
        <!--padding-left: 1%;-->
        <!--font-size: 13px;-->
        <!--margin-top: 1%;-->
        <!--color:black;">-->
          <!---->
      <!--<span *ngIf="name" style="-->
      <!--padding: 5px; -->
      <!--background: linear-gradient(to bottom right, #6ac0e3, #46bbb1); -->
      <!--border-radius: 5px;-->
      <!--padding: 5px;-->
      <!--padding-left: 15px;-->
      <!--padding-right: 15px;-->
      <!--margin-left: -1%;">{{name}}</span> &nbsp;&nbsp;-->
          <!--<mat-form-field class="example-form-field" style="font-size: 11px;margin-bottom: 1%">-->
            <!--<span matPrefix><i class="material-icons">search</i>&nbsp;&nbsp;&nbsp;</span>-->
            <!--<input matInput type="text" placeholder="Search Parameter" (keyup)="onQuickFilterChanged($event)"/>-->
          <!--</mat-form-field>-->
        <!--</div>-->
      <!--</div>-->
    <!--</div>-->
    
    <mat-toolbar style="background: white">
      <mat-toolbar-row>
        <span style="font-size: 19px;">{{name}}</span>
        <span class="example-spacer"></span>
        <form class="cam-form">

          <mat-form-field class="cam-full-width">
            <input type="text" matInput placeholder="Search Parameter" (keyup)="onQuickFilterChanged($event)">
            <mat-icon matSuffix>search</mat-icon>
          </mat-form-field>

        </form>
      </mat-toolbar-row>
    </mat-toolbar>

    <ag-grid-angular
      #agGrid
      style="width: 100%"
      [style.height.px]="height"
      class="ag-theme-balham"
      [rowData]="data"
      [columnDefs]="colDefs"
      [enableSorting]="true"
      [enableFilter]="true"
      [rowSelection]="rowSelection"
      (selectionChanged)="onSelectionChanged()"
      [enableColResize]="true"
      [frameworkComponents]="frameworkComponent"
      [gridOptions]="gridOptions"
      (gridReady)="onGridReady($event)">
    </ag-grid-angular>
  `,
  styles: [`
    .example-spacer {
      flex: 1 1 auto;
    }

    .cam-form {
      min-width: 150px;
      max-width: 380px;
      width: 100%;
    }

    .cam-full-width {
      width: 100%;
      font-size: 18px;
      color: #0000008f;
      font-family: sans-serif;
    }
  `]
})
export class AgGridTableComponent {
  gridApi;
  gridColumnApi;
  rowSelection;

  public gridOptionsT: GridOptions;
  public gridOptionsN: GridOptions;


  @Input() name: any;
  @Input() fit: any;
  @Input() height: any;
  @Input() width: any;
  @Input() data: any;
  @Input() colDefs: AgHeaders[];
  @Input() createTicket: boolean;
  @Input() icon: string;
  @Input() label: string;


  @Input() dataT: any;
  @Input() heightT: number;
  @Input() rowClick: boolean;
  @Input() needPopUp: boolean;
  @Input() styleGrid: boolean;
  @Input() alertDataTBS: any;
  @Input() alertDataBRN: any;

  @Input() hidden: boolean;
  @Input() show: boolean;
  @Input() showB: boolean;
  @Input() SgsnChart: any;
  @Input() GgsnChart: any;
  @Input() chartDown: any;
  @Input() type: any;

  @Input() gridOptions: any;
  @Input() frameworkComponent: any;

  // TAMBAH
  dataNodin: any;
  // END TAMBAH


  constructor(private store: Store<any>, public dialog: MatDialog) {
    this.gridOptionsT = <GridOptions>{};
    this.gridOptionsN = <GridOptions>{};
    this.refreshData();
    this.onGridReady('params');
    this.rowSelection = 'single';

  }

  sizeToFit() {
    this.gridApi.sizeColumnsToFit();

  }

  onQuickFilterChanged($event) {
    this.gridApi.setQuickFilter($event.target.value);
  }


  onGridReady(params) {
    if (this.fit) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      params.api.sizeColumnsToFit();
      this.gridApi.sizeColumnsToFit();
      params.api.sizeColumnsToFit();
      window.addEventListener('resize', function () {
        setTimeout(function () {
          params.api.sizeColumnsToFit();
        });
      });
    }
    else {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      window.addEventListener('resize', function () {
        setTimeout(function () {
          // params.api.sizeColumnsToFit();
        });
      });
    }
  }

  refreshData() {
  }

  onSelectionChanged() {
  }
}
