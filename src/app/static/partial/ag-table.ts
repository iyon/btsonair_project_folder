import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import { FeatureService } from '../provider/feature-service';
import { filter, map } from 'rxjs/operators';
import { storeFreeze } from 'ngrx-store-freeze';
import { Store } from '@ngrx/store';
import { ActionSettingsStatusId, ActionSettingsStatusName, selectorSettings } from '../../settings/settings.reducer';
export interface FileUpload{
  name: string,
  file: File
}
export const statuss = [];
export interface UpdateStat{
  id: number,
  name: string
}
export const newRoww = [];
export interface UpdateRow{
  field: string,
  value: string
}


@Component({
  selector: 'ag-table',
  template: `
    <ag-grid-angular
    #agGrid
    style="width: 100%;height: 514px;margin-top: 5%;"
    id="myGrid"
    gridOptions.rowHeight = 50
    [rowData]="rowData"
    class="ag-theme-material"
    [columnDefs]="columnDefs"
    [frameworkComponents]="frameworkComponents"
    [enableColResize]="true"
    [enableSorting]="true"
    [animateRows]="true"
    [multiSortKey]="multiSortKey"
    [sortingOrder]="sortingOrder"
    [masterDetail]="true"
    (gridReady)="onGridReady($event)"
    [defaultColDef]="defaultColDef"
    [rowSelection]="rowSelection"
    [rowDeselection]="true"
    [suppressRowClickSelection]="true"
    [suppressMenuHide]="true"
    [isRowSelectable]="isRowSelectable"
    [enableFilter]="true"
    [pagination]="true"   
    (cellValueChanged)="onCellValueChanged($event)"
    [detailCellRendererParams]="detailCellRendererParams"
    [colResizeDefault]="colResizeDefault"
    
    
  >
  </ag-grid-angular>
        <button type="button" mdbBtn color="deep-orange" class="relative waves-light" 
    mdbWavesEffect (click)="onInsertRowAt2()" style="margin-top: -1%;">Insert New Row</button>
   

<!-- [paginationAutoPageSize]="true" 

-->
  
  `,

  // styleUrls: ['./features.component.scss']
})



export class AgTable implements OnInit {
routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
versions = env.versions;
columnDefs;
defaultColDef;
rowData;
frameworkComponents;
sortingOrder;
multiSortKey;
gridApi;
gridColumnApi;
detailCellRendererParams;
rowSelection;
isRowSelectable;
files: FileUpload[] = [];
@ViewChild('myInput1')
myInputVariable1: any;
refresh : boolean;
listStatus: any;
token: any;
colResizeDefault;



  constructor(private http: HttpClient, private featureService: FeatureService, private store: Store<any>) {
    
    this.columnDefs = [
      { 
        headerName: "REGIONAL",
        field: "regional",
        width: 165,
        cellRenderer: "agGroupCellRenderer",
        suppressSizeToFit: true
      },
      { 
        headerName: "VENDOR",
        field: "vendor",
        width: 110, 
        suppressSizeToFit: true
      },
      {
        headerName: "BTS NODE NAME",
        field: "bts_node_name",
        width: 170,
        suppressSizeToFit: true
      },
      {
        headerName: "NE ID",
        field: "ne_id",
        width: 105,
        suppressSizeToFit: true,
      },
      {
        headerName: "SITE ID",
        field: "site_id",
        width: 100,
        suppressSizeToFit: true
      },
      {
        headerName: "FREQ",
        field: "freq",
        width: 100,
        suppressSizeToFit: true
      },
      {
        headerName: "DAFINCI",
        field: "achv_date",
        width: 110,
        cellStyle: {'background-color': '#00E5FF'},
        suppressSizeToFit: true
      },
      {
        headerName: "TRAFFIC",
        field: "traffic",
        width: 110,
        cellStyle: {'background-color': '#9bc2e6'},
        suppressSizeToFit: true
        
      },
      {
        headerName: "REVENUE",
        field: "revenue",
        width: 110,
        cellStyle: {'background-color': '#9575cd'},
        suppressSizeToFit: true
      },
      { 
        headerName: "REMEDY",
        field: "remedy",
        width: 110,
        cellStyle: {'background-color': '#d81b60'},
        suppressSizeToFit: true
      },
      {
        headerName: "DESC",
        field: "description",
        width: 110,
        suppressSizeToFit: true
      },
      {
        headerName: "STATUS",
        field: "status",
        width: 100,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: {
          cellHeight: 50,
          values: [
            "ON AIR - NEW",
            "ON AIR - EXISTING",
            "ON AIR - SWADAYA", 
            "ON AIR - NON REPORTED",
            "ON AIR - RELOKASI",
            "CONNECTED",
            "SWAP / MODERNISASI",
            "OFF AIR",
            "DB PLAN",
            "DISMANTLE",  
            "DATABASE SAMPAH",
          ]
        },
        suppressSizeToFit: true,
      },
      {
        headerName: "SUBMITTED BY",
        field: "submitted_by",
        width: 100,
        suppressSizeToFit: true
      },
      {
        headerName: "APPROVAL STATUS",
        field: "aprstatus",
        width: 100,
        suppressSizeToFit: true,
      },  
    ];
    this.store.select(selectorSettings).subscribe(x => {
      this.refresh = x.statusName
      if(this.refresh == true){
        this.gridApi.refreshCells({force: true});
        console.log("refresh true",this.refresh)
        this.store.dispatch(new ActionSettingsStatusName({statusName: false}))    
        this.refresh = false
        console.log("refresh false",this.refresh)
      }
    })
  this.defaultColDef = { editable: function(param){
    var paramstat = param.data.status;
    if(paramstat !== "NEW REKON"){ //<-- set editable col status only
      if (param.colDef.field == "status"){
        return true 
    }}
    else if(paramstat == "NEW REKON"){
      if(param.colDef.field == "status"){
        return false
      }
      return true
    }else {
      return false
    }
  }};
  
  this.rowSelection = "multiple";

  

    this.detailCellRendererParams = {
      detailGridOptions: {
        columnDefs: [
        {
            field: "SOURCES",
        },
        { 
            field: "REGIONAL" 
        },
        {
            field: "VENDOR",
        },
        { 
            field: "RNCID_LACCI" 
        },
        { 
            field: "RNC_NAME" 
        },
        { 
            field: "NODEB_NAME" 
        },
        { 
            field: "CELL_NAME" 
        },
        { 
            field: "CELL_NO" 
        },
        { 
            field: "NE_ID" 
        },
        { 
            field: "SITE_ID" 
        },
        {
            field: "HSDPA"
        },
        { 
            field: "LAC" 
        },
        { 
            field: "CI" 
        },
        { 
            field: "SAC" 
        },
        { 
            field: "PSCCODE" 
        },
        { 
            field: "FREQUENCY" 
        },
        { 
            field: "F1_F2_F3" 
        },
        { 
            field: "TYPE_BTS" 
        },
        { 
            field: "NEW_SITE" 
        },
        { 
            field: "LONGITUDE" 
        },
        { 
            field: "LATITUDE" 
        },
        { 
            field: "STATUSE" 
        },
        {
            headerName: "BTS NUMBER",
            field: "BTS_NUMBER" 
        },
        {
            headerName: "CELL NUMBER", 
            field: "CELL_NUMBER" 
        },
        { 
            headerName: "CLUSTER SALES", 
            field: "CLUSTER_SALES" 
        },
        { 
            field: "NSA" 
        },
        { 
            field: "RTPO" 
        },
        { 
            field: "ALAMAT" 
        },
        { 
            field: "KELURAHAN" 
        },
        { 
            field: "KECAMATAN" 
        },
        { 
            field: "KABUPATEN" 
        },
        { 
            field: "PROVINSI" 
        },
        { 
            headerName: "OWNER SITE",
            field: "OWNER_SITE" 
        },
        { 
            headerName: "NAMA TP",
            field: "NAMA_TP" 
        },
        { 
            headerName: "STATUS PLN",
            field: "STATUS_PLN" 
        },
        { 
            headerName: "NE CLASS",
            field: "NE_CLASS" 
        },
        ],
        onFirstDataRendered(params) {
          params.api.sizeColumnsToFit();
        }
      },
      getDetailRowData: function(params) {
        params.successCallback(params.data.callRecords);
      }
    };
  }


onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
  // 
  params.api.sizeColumnsToFit();
  // 

  this.http
  // DUMMY
    // .get(
    //   "https://raw.githubusercontent.com/ag-grid/ag-grid-docs/latest/src/javascript-grid-master-detail/simple/data/data.json"
    // )

    // SERVER BENER
    .get(
        "http://10.54.36.49/api-btsonair/public/api/v1/nodeb?token=" + this.token
      )

      // LOCAL ABDUR
    // .get(
    //     "http://10.47.198.153/btsonair/public/api/v1/nodeb?token=" + this.token 
    //   )

    // LOCAL NANDA 
    // .get(
    //   "http://172.16.0.107/apis-btsonair/public/api/v1/nodeb?token=" + this.token
    // )

    .subscribe(data => {
      this.rowData = data;

    });



  setTimeout(function() {
    var rowCount = 0;
    params.api.forEachNode(function(node) {
      node.setExpanded(rowCount++ === 1);
    });
  }, 500);

  
  
}


  ngOnInit() {
    this.token = sessionStorage.getItem("token")
    if (sessionStorage.getItem('token')) {
      console.log("dapat session storage", sessionStorage.getItem('token')); //converts to json object
    } else {
      window.location.assign('http://10.54.36.49/landingPage/')
    //   // window.location.assign('http://10.47.198.153/landingPage/public')
    //   // window.location.assign('http://172.16.7.111/landingPage/public')
      console.log('key dose not exists');
    }

  }

  openLink(link: string) {
    window.open(link, '_blank');
  }

  fileUpload(event: any) {
    console.log('event scdi', event)
    let files = event.target.files
    Array.from(files).forEach(element => {
      this.files.push({
        name: 'uploadFileNodin',
        file: <File> element
      })
    } )
  }



  

  onCellValueChanged(params) {
    
    
    var paramstat = params.data.status;
    var newValue = params.newValue;    

    if(paramstat == "NEW REKON"){ 
      let colId = params.column.getId();
      let newDatas = {"field": colId,"value":newValue};
      if(newRoww.length !== 0){               
        let tes = newRoww.filter(x => x.field == colId)
        let contain = newRoww.indexOf(tes[0])        
        if(contain !== -1){
          newRoww.splice(contain, 1)          
        }
      }
      newRoww.push(newDatas);
      console.log("newRoww", newRoww)
    }else{
      let id = params.data.ne_id;
      let data = {"id"  : id,"name" : newValue}
      if(statuss.length !== 0){               
        let tes = statuss.filter(x => x.id == id)
        let contain = statuss.indexOf(tes[0])        
        if(contain !== -1){
          statuss.splice(contain, 1)          
        }
      }
      statuss.push(data); 
      console.log("statuss", statuss)
    }
       
  }
  

  onInsertRowAt2() {
    var newItem = createNewRowData();
    var res = this.gridApi.updateRowData({
      add: [newItem],
      addIndex: 0
    });
    printResult(res);
  }

  onAddRow() {
    var newItem = createNewRowData();
    var res = this.gridApi.updateRowData({ add: [newItem] });
    printResult(res);
  }

}

var newCount = 1;
function createNewRowData() {
  var newData = {
    status: "NEW REKON",
  };
  newCount++;
  return newData;
}
function printResult(res) {
  console.log("---------------------------------------");
  if (res.add) {
    res.add.forEach(function(rowNode) {
      console.log("Added Row Node", rowNode);
    });
  }
  if (res.remove) {
    res.remove.forEach(function(rowNode) {
      console.log("Removed Row Node", rowNode);
    });
  }
  if (res.update) {
    res.update.forEach(function(rowNode) {
      console.log("Updated Row Node", rowNode);
    });
  }
}



