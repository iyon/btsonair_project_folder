import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import { FeatureService } from '../provider/feature-service';
import { filter, map } from 'rxjs/operators';
import { storeFreeze } from 'ngrx-store-freeze';
import { Store } from '@ngrx/store';
import { ActionSettingsStatusId, ActionSettingsStatusName, selectorSettings, ActionSettingsListName, ActionSettingsListNE } from '../../settings/settings.reducer';
export interface FileUpload{
  name: string,
  file: File
}

export interface NodinUpload{
  list: any,
  file: File
}

export const cekliss = [];

@  Component({
  selector: 'modal-table',
  template: `
  <ag-grid-angular
  #agGrid
  style="width: 100%;height: 418px;"
  id="myGrid"
  [rowData]="rowData"
  class="ag-theme-material"
  [columnDefs]="columnDefs"
  [frameworkComponents]="frameworkComponents"
  [enableColResize]="true"
  [enableSorting]="true"
  [animateRows]="true"
  [multiSortKey]="multiSortKey"
  [sortingOrder]="sortingOrder"
  [masterDetail]="true"
  [detailCellRendererParams]="detailCellRendererParams"
  (gridReady)="onGridReady($event)"
  [defaultColDef]="defaultColDef"
  [rowSelection]="rowSelection"
  [rowDeselection]="true"
  [rowMultiSelectWithClick]="true"
  (selectionChanged)="onSelectionChanged($event)"
  [suppressMenuHide]="true"
  [isRowSelectable]="isRowSelectable"
  [enableFilter]="true"
  [pagination]="true"


  
>
  
</ag-grid-angular>

<!--[paginationAutoPageSize]="true" -->
  
 
  `,

  // styleUrls: ['./features.component.scss']
})



export class ModalTable implements OnInit {
routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
versions = env.versions;
columnDefs;
defaultColDef;
rowData;
frameworkComponents;
sortingOrder;
multiSortKey;
gridApi;
gridColumnApi;
detailCellRendererParams;
rowSelection;
isRowSelectable;
files: FileUpload[] = [];
items: NodinUpload[] = [];
@ViewChild('myInput1')
  myInputVariable1: any;
listNe = [];
refresh : boolean;
listStatus: any;
token: any;
  constructor(private http: HttpClient, private featureService: FeatureService,public store: Store<any>) {
    this.columnDefs = [
      // {
      //   field: "id",
      //   width: 110, 
      //   cellRenderer: "agGroupCellRenderer"
      // },
      // {
      //   headerName: "Cheklist",
      //     field: "cheklist",
      //     width: 120,
      //     headerCheckboxSelection: false,
      //     checkboxSelection: true
      //   },
      // {
      //   headerName: "NE ID",
      //   field: "NEID",
      //   width: 170,
      // },
      {
        headerName: "NE ID",
        field: "ne_id",
        width: 350,
        headerCheckboxSelection: false,
        checkboxSelection: true

      },


    ];



    this.store.select(selectorSettings).subscribe(x => {
      this.refresh = x.listName
      if(this.refresh == true){
        this.gridApi.refreshCells({force: true});
        console.log("refresh true",this.refresh)
        this.store.dispatch(new ActionSettingsListName({listName: false}))    
        this.refresh = false
        console.log("refresh false",this.refresh)
      }
    })




  // this.frameworkComponents = { genderCellRenderer: GenderCellRenderer };
  this.defaultColDef = { editable: true };
  this.rowSelection = "multiple";

  
}

  
  

 

onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;

  // 
  params.api.sizeColumnsToFit();
  // 

  this.http
    // .get(
    //   "https://raw.githubusercontent.com/ag-grid/ag-grid-docs/latest/src/javascript-grid-master-detail/simple/data/data.json"
    // )
    .get(
      "http://10.54.36.49/api-btsonair/public/api/v1/nodeb?token=" + this.token // SERVER BENER
      )
      
    // .get(
    //   "http://10.47.198.153/btsonair/public/api/v1/nodeb?token=" + this.token  // ABDUR PUNYA
    // )  

    // .get(
    //   "http://172.16.7.111/api-btsonair/public/api/v1/nodeb?token=" + this.token
    // )

    .subscribe(data => {
      this.rowData = data;
    });

  setTimeout(function() {
    var rowCount = 0;
    params.api.forEachNode(function(node) {
      node.setExpanded(rowCount++ === 1);
    });
  }, 500);

  
}


ngOnInit() {
  this.token = sessionStorage.getItem("token")
  // if (sessionStorage.getItem('token')) {
  //   console.log("dapat session storage", sessionStorage.getItem('token')); //converts to json object
  // } else {
  //   window.location.assign('http://10.54.36.49/landingPage/')
  //   console.log('key dose not exists');

  // }
}

  openLink(link: string) {
    window.open(link, '_blank');
  }

  


onSelectionChanged(event) {
  var rowCount = event.api.getSelectedNodes().length;
  var list = Array();
  for(var _i = 0; _i < rowCount; _i++){
    var row = event.api.getSelectedNodes()[_i].data
    // list.push(row.bts_node_name)    
    list.push(row.ne_id)
    // console.log("bts_node_name", event.api.getSelectedNodes()[_i].data)
  }  
  // window.alert("selection changed, " + rowCount + " rows selected");
  console.log("list ne", list)
  this.store.dispatch(new ActionSettingsListNE({listne: list})) //
}

}


