/* tslint:disable:no-any no-magic-numbers prefer-function-over-method */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { getBasicData } from './data';
import * as Handsontable from 'handsontable';
import { HttpClient } from '@angular/common/http';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'basic-demo',
  // templateUrl: './hands-table.html'
  template: 
  `
  <div>
  <hot-table
    class="hot"
    [data]="dataset"
    [colHeaders]="true"
    [rowHeaders]="true">
      <hot-column data="regional" [readOnly]="true" title="Regional"></hot-column>
      <hot-column data="vendor" title="Vendor"></hot-column>
      <hot-column data="ne_id" title="NE ID"></hot-column>
  </hot-table>
</div>

<!--
<div id="hot"></div>

-->`
})


export class BasicDemoComponent  {

  dataset;
  token: any;
  gridApi;
  gridColumnApi;
  constructor(private http: HttpClient,){

    this.token = sessionStorage.getItem("token") 
    console.log("dapat loh", this.token)
    this.http  
      .get(
          "http://10.54.36.49/api-btsonair/public/api/v1/nodeb?token=" + this.token
        )  
      .subscribe(data => {
        this.dataset = data;
        })

 

//   var hotElement = document.getElementById('#hot');
// var hotElementContainer = hotElement.parentNode;
// var hotSettings = {
//   data: this.dataset,
//   columns: [
//     {
//       data: 'ne_id',
//       type: 'numeric',
//       width: 40
//     },
//     {
//       data: 'vendor',
//       // renderer: flagRenderer
//       type: 'text'
//     },
//     {
//       data: 'regional',
//       type: 'text'
//     },
//     {
//       data: 'bts_node_name',
//       type: 'text'
//     },
//     {
//       data: 'site_id',
//       type: 'numeric',
//       numericFormat: {
//         pattern: '0.0000'
//       }
//     },
//     {
//       data: 'freq',
//       type: 'text'
//     },
//     {
//       data: 'achv_date',
//       type: 'date',
//       dateFormat: 'MM/DD/YYYY'
//     },
//     {
//       data: 'traffic',
//       type: 'numeric',
//       // numericFormat: {
//       //   pattern: '0.00%'
//       // }
//     }
//   ],
//   stretchH: 'all',
//   width: 806,
//   autoWrapRow: true,
//   height: 487,
//   maxRows: 22,
//   manualRowResize: true,
//   manualColumnResize: true,
//   rowHeaders: true,
// colHeaders: [
//   'NE ID',
//   'VENDOR',
//   'REGIONAL',
//   'BTS NODE NAME',
//   'SITE ID',
//   'FREQ',
//   'DAFINCI',
//   'TRAFFIC'
// ],
// manualRowMove: true,
//   manualColumnMove: true,
//   contextMenu: true,
//   columnSorting: {
//     indicator: true
//   },
//   autoColumnSize: {
//     samplingRatio: 23
//   },
//   fixedRowsTop: 2,
//   fixedRowsBottom: 2,
//   fixedColumnsLeft: 3
// };
// var hot = new Handsontable(hotElement, hotSettings);


  }
    // ngOnInit() {
    //   // this.token = sessionStorage.getItem("token")  
    //   // console.log("dapat loh", this.token)
    // }
}



