import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {GridOptions} from "ag-grid";
import {ICellRendererAngularComp} from "ag-grid-angular";
import { FeatureService } from "../provider/feature-service";

@Component({
  selector: 'nodin',
  template: `

    <mat-toolbar color="primary" style="border-radius: 5px 5px 0 0;">
      <mat-toolbar-row>
        <span>Nodin</span>
        <span class="example-spacer"></span>
        <button mat-icon-button style=" color: #b3b3b3;" (click)="closeDialog()">
          <mat-icon aria-label="Close">close</mat-icon>
        </button>
      </mat-toolbar-row>
    </mat-toolbar>

    <mat-card>
      <!--<mat-card-header>-->
      <!---->
      <!--</mat-card-header>-->

      <mat-card-content>
        <div class="row">
          <div class="col-md-12">
          <ag-grid-table
              [data]="dataNodin"
              [name]="'Nodin Table'"
              [gridOptions]="gridOptions"
              [height]="280"
              [frameworkComponent]="frameworkComponents">
            </ag-grid-table>
          </div>
        </div>
      </mat-card-content>

      <mat-card-actions style="width: 100%">
        <div class="row">
          <div class="col-md-9" style="margin-left: 5.3%;">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="fileDipilih" #file (change)="fileChange(file)">
              <label class="custom-file-label" for="fileDipilih">{{selectedFile}}</label>
            </div>
          </div>
          <div class="col-md-2">
            <button mat-raised-button color="primary">
              <mat-icon>publish</mat-icon>
              Upload
            </button>
          </div>
        </div>
      </mat-card-actions>
    </mat-card>
  `,
  styles: [`
    .example-spacer {
      flex: 1 1 auto;
    }
  `]
})
export class nodinComponent {
  public gridOptions: GridOptions;
  public frameworkComponents: any;
  dummyData: any;
  dataNodin: any;
  selectedFile = "Choose File";

  constructor(public dialogRef: MatDialogRef<nodinComponent>, public featureService: FeatureService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = this.createColumnDefs();

    let x = data.datas;
    this.dataNodin = x;
    console.log('sadawda', x)

    this.dummyData = [{
      'contoh': 'asdhkw',
      'coba': 'snsajdwhwuqhrwjp'
    }];

    this.frameworkComponents = {
      download: downloadNodin,
      delete: deleteNodin,
    };
  }

  private createColumnDefs() {
    return [
      {
        headerName: 'File Name', field: 'name', width: 255,
      },
      {
        headerName: 'File Date', field: 'date', width: 220
      },
      {
        headerName: 'Download', field: 'download', width: 220, cellRenderer: 'download'
      },
      {
        headerName: 'Delete', field: 'delete', width: 220, cellRenderer: 'delete'
      }
    ]
  }

  fileChange(file: HTMLInputElement) {
    let fileSelected = file.files[0].name;
    //console.log(fileSelected);
    // let last_slash_index = fileSelected_tmp.lastIndexOf("\\");
    // let fileSelected = fileSelected_tmp.substring(last_slash_index + 1, fileSelected_tmp.length);
    this.selectedFile = fileSelected;
  }

  methodFromParent(cell) {
    alert("Parent Component Method from " + cell + "!");
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}


/**
 *  Cell Renderer For Download Button
 */

@Component({
  selector: 'downloadNodin',
  template: `
    <span><button style="height: 20px" class="btn btn-info">Download</button></span>
  `,
  styles: [`
    .btn {
      line-height: 0.5
    }
  `]
})

export class downloadNodin implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }
}


/**
 * Cell Renderer For Delete Button  
 */

@Component({
  selector: 'deleteNodin',
  template: `
    <span><button style="height: 20px" class="btn btn-info">Delete</button></span>
  `,
  styles: [`
    .btn {
      line-height: 0.5
    }
  `]
})

export class deleteNodin implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }
}
