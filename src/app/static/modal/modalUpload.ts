import { Component, OnInit, Input } from '@angular/core';
// import { IDriver, dLocation } from '../interfaces';

// import { NgbModal, ModalDismissReason } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from "@angular/common/http";
import { IDriver, dLocation } from '../../interfaces';
import { DriversListComponent } from '../drivers-list.component';
import { DataService } from './modalUpload.service';
import { FeatureService } from '../provider/feature-service';
import { filter } from 'rxjs/operators';
import { ActionSettingsListNE, selectorSettings } from '../../settings/settings.reducer';
import { Store } from '@ngrx/store';
export interface FileUpload{
  name: string,
  file: File
}

export interface NodinUpload{
  list: any,
  file: File
}



@Component({
  selector: 'dr-modal',
  templateUrl: './modalUpload.html',
  styleUrls: ['./modalUpload.css']
})
export class LukeComponent implements OnInit {
    lokasi: any[];
  // biodata = document.querySelector('#luke');
  lukeName: any;
  lukeBirth: any;
  columnDefs;
defaultColDef;
rowData;
frameworkComponents;
sortingOrder;
multiSortKey;
gridApi;
gridColumnApi;
detailCellRendererParams;
rowSelection;
isRowSelectable;
files: FileUpload[] = [];
items: NodinUpload[] = [];
listNe = Array();

  loading: boolean = true;
  
  constructor(private http: HttpClient, private featureService: FeatureService,public store: Store<any>) {
    
  }
  


  openModal(){
    //  this.modalService.getModal('<modal-table></modal-table>')
    console.log("mantap")
    // this.modalService.getModal('http://10.54.36.49/btsonair/apis/public/api/v1/nodeb').subscribe(x => {
      
      
    //   this.loading = false;


    // })


    document.getElementById("nodinboy").style.opacity = "1";

   }

   
  fileUpload(event: any) {
    console.log('event scdi', event)
    let files = event.target.files
    Array.from(files).forEach(element => {
      this.files.push({
        name: 'uploadFileNodin',
        file: <File> element
      })
    } )
  }

  upload() {
    // console.log("klik")
    // // if (this.files.length > 0) {
    //   console.log('upload this.files', this.files)
    //   this.store.dispatch(this.uploadActions.setHuaweiTableStatus('loading'))
      this.featureService.uploadFiles(this.files).subscribe(data => {
        // this.store.dispatch(this.uploadActions.setHuaweiTableStatus('loaded'))
        // this.store.dispatch(this.uploadActions.setHuaweiTable(data))
        // this.myInputVariable1.value = "";
        // this.myInputVariable2.nativeElement.value = "";
        // this.myInputVariable3.nativeElement.value = "";
        // this.myInputVariable4.nativeElement.value = "";
        // this.myInputVariable5.nativeElement.value = "";
        // this.myInputVariable6.nativeElement.value = "";
        // this.myInputVariable7.nativeElement.value = "";
        this.files = []
        alert("Document Submitted")
      },
    error => {
      alert("ada error")
      // this.store.dispatch(this.uploadActions.setHuaweiTableStatus('loaded'))
      // this.store.dispatch(this.uploadActions.setHuaweiTable(this.message))
      // this.myInputVariable1.value = "";
      // this.myInputVariable2.nativeElement.value = "";
      // this.myInputVariable3.nativeElement.value = "";
      // this.myInputVariable4.nativeElement.value = "";
      // this.myInputVariable5.nativeElement.value = "";
      // this.myInputVariable6.nativeElement.value = "";
      // this.myInputVariable7.nativeElement.value = "";
      this.files = []
    })
}

// onRowSelected(event) {
//   this.listNe.push(event.node.data.ne_id)
//   this.store.dispatch(new ActionSettingsListNE({ listne: this.listNe }));
//   this.store.select(selectorSettings).subscribe(data => {
      
//     console.log("list Nedin1",data.listne)
  
//    })
//   // window.alert("row " + event.node.data.Checklist + " selected = " + event.node.selected);
//   window.alert(this.listNe);
// }

  submitNedin(){
    this.store.select(selectorSettings).subscribe(data => {
      this.listNe = data.listne
    })
    this.featureService.nodinNe(this.files, this.listNe).subscribe(
      data => {
        alert("Document Submitted")
      },
      error => {
        alert("ada error")
      }
    )
    console.log("list Nedin",this.listNe)
  }


  ngOnInit() {


  }


}
