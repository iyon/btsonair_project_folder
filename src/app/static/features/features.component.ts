import { Component, OnInit, ViewChild } from '@angular/core';

import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import {FeatureService} from '../provider/feature-service';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { selectorSettings } from '../../settings';
import { statuss, newRoww } from '../partial/ag-table';
import { async } from '@angular/core/testing';
import { ActionSettingsStatusName } from '../../settings/settings.reducer';

@Component({
  selector: 'anms-features',
  template: `
    <ag-table></ag-table>
    <dr-modal></dr-modal> 
    <button type="button" mdbBtn color="default" class="relative waves-light" 
    mdbWavesEffect (click)="updateStatus()" style="margin-top: -1%;">Save Changes</button>
    <unk-table></unk-table>
    <test-table></test-table>
    
    <!-- <basic-demo></basic-demo> 
    -->  
  `,

  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;

  constructor(private http: HttpClient, private featureService: FeatureService,public store: Store<any>) {

  }
  // onRowSelected(event) {
//   this.listNe.push(event.node.data.ne_id)
//   this.store.dispatch(new ActionSettingsListNE({ listne: this.listNe }));
//   this.store.select(selectorSettings).subscribe(data => {
      
//     console.log("list Nedin1",data.listne)
  
//    })
//   // window.alert("row " + event.node.data.Checklist + " selected = " + event.node.selected);
//   window.alert(this.listNe);
// }

  ngOnInit() {}

  openLink(link: string) {
    window.open(link, '_blank');
  }

  updateStatus(){ 

    this.featureService.changeStatus(statuss).subscribe(
      data => {        
        console.log("response", data)
        alert("submit success")
      },
      error => {
        alert("error")
      }
    )
    this.featureService.changeRow(newRoww).subscribe(
      data => {        
        console.log("response", data)
        alert("submit success")
      },
      error => {
        alert("error")
      }
    )
    // this.store.dispatch(new ActionSettingsStatusName({statusName: true}))
  }

  
}
