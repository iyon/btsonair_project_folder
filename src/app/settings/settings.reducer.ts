import { Action } from '@ngrx/store';
import { UpdateStat } from '../static/partial/ag-table';

export const SETTINGS_KEY = 'SETTINGS';

export enum SettingsActionTypes {
  CHANGE_THEME = '[Settings] Change Theme',
  CHANGE_AUTO_NIGHT_AUTO_MODE = '[Settings] Change Auto Night Mode',
  CHANGE_ANIMATIONS_PAGE = '[Settings] Change Animations Page',
  CHANGE_ANIMATIONS_PAGE_DISABLED = '[Settings] Change Animations Page Disabled',
  CHANGE_ANIMATIONS_ELEMENTS = '[Settings] Change Animations Elements',
  PERSIST = '[Settings] Persist',
  LISTNE = '[Setting] listNe',
  LIST_NAME = '[Setting] add List Name',
  STATUS_ID = '[Setting] Change Status Id',
  STATUS_NAME = '[Setting] Change Status Name'

}

export class ActionSettingsChangeTheme implements Action {
  readonly type = SettingsActionTypes.CHANGE_THEME;
  constructor(public payload: { theme: string }) {}
}

export class ActionSettingsChangeAutoNightMode implements Action {
  readonly type = SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE;
  constructor(public payload: { autoNightMode: boolean }) {}
}

export class ActionSettingsChangeAnimationsPage implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_PAGE;
  constructor(public payload: { pageAnimations: boolean }) {}
}

export class ActionSettingsChangeAnimationsPageDisabled implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED;
  constructor(public payload: { pageAnimationsDisabled: boolean }) {}
}

export class ActionSettingsChangeAnimationsElements implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS;
  constructor(public payload: { elementsAnimations: boolean }) {}
}

export class ActionSettingsPersist implements Action {
  readonly type = SettingsActionTypes.PERSIST;
  constructor(public payload: { settings: SettingsState }) {}
}

export class ActionSettingsStatusId implements Action {
  readonly type = SettingsActionTypes.STATUS_ID;
  constructor(public payload: { statusId: any }) {}
}

export class ActionSettingsStatusName implements Action {
  readonly type = SettingsActionTypes.STATUS_NAME;
  constructor(public payload: { statusName: boolean }) {}
}
export class ActionSettingsListName implements Action {
  readonly type = SettingsActionTypes.LIST_NAME;
  constructor(public payload: { listName: boolean }) {}
}

export class ActionSettingsListNE implements Action {
  readonly type = SettingsActionTypes.LISTNE;
  constructor(public payload: { listne: any}) {}
}

export type SettingsActions =
  | ActionSettingsPersist
  | ActionSettingsChangeTheme
  | ActionSettingsChangeAnimationsPage
  | ActionSettingsChangeAnimationsPageDisabled
  | ActionSettingsChangeAnimationsElements
  | ActionSettingsChangeAutoNightMode
  | ActionSettingsStatusId
  | ActionSettingsStatusName
  | ActionSettingsListNE
  | ActionSettingsListName;
  

export const NIGHT_MODE_THEME = 'BLACK-THEME';

export const initialState: SettingsState = {
  theme: 'BLACK-THEME',
  autoNightMode: false,
  pageAnimations: true,
  pageAnimationsDisabled: false,
  elementsAnimations: true,
  listne: Array(),
  statusId: 0,
  statusName: false,
  listName: false,
};

export const selectorSettings = state =>
  <SettingsState>(state.settings || { theme: '' });

export function settingsReducer(
  state: SettingsState = initialState,
  action: SettingsActions
): SettingsState {
  switch (action.type) {
    case SettingsActionTypes.CHANGE_THEME:
      return { ...state, theme: action.payload.theme };

    case SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE:
      return { ...state, autoNightMode: action.payload.autoNightMode };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE:
      return { ...state, pageAnimations: action.payload.pageAnimations };

    case SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS:
      return {
        ...state,
        elementsAnimations: action.payload.elementsAnimations
      };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED:
      return {
        ...state,
        pageAnimations: false,
        pageAnimationsDisabled: action.payload.pageAnimationsDisabled
      };

      case SettingsActionTypes.STATUS_ID:
      return { ...state, statusId: action.payload.statusId };

      case SettingsActionTypes.STATUS_NAME:
      return { ...state, statusName: action.payload.statusName };

      case SettingsActionTypes.LISTNE:
      return { ...state, listne: action.payload.listne };

      case SettingsActionTypes.LIST_NAME:
      return { ...state, listName: action.payload.listName };

    default:
      return state;
  }
}

export interface SettingsState {
  theme: string;
  autoNightMode: boolean;
  pageAnimations: boolean;
  pageAnimationsDisabled: boolean;
  elementsAnimations: boolean;
  listne: any;
  listName: boolean;
  statusId: any;
  statusName: boolean;
}
